<?php
namespace Cyphp;

class Str
{
    public static function gbkToUtf8($html)
    {
        $encodeList = mb_detect_encoding($html, ['ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5']);
        $tmpHtml = mb_convert_encoding($html, "UTF-8", $encodeList);
        return $tmpHtml;
    }
}