<?php
namespace Cyphp;

class Http
{
    public static function curlGet($url, $customHeader = [], $proxyUrl = [])
    {
        $header = [];

        if (!empty($customHeader)){
            $header = array_merge($customHeader, $header);
        }else{
            $header = ['User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'];
        }

        $curl = curl_init();

        if(!empty($header)){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_HEADER, 0);//返回response头部信息
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_ENCODING, '');

        if (!empty($proxyUrl)){
            curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);

            if (stristr($proxyUrl['ip'], 'http')){
                curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
            }else{
                curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
            }

            curl_setopt($curl, CURLOPT_PROXY, str_replace(['http://', 'https://'], [''], $proxyUrl['ip']));
            curl_setopt($curl, CURLOPT_PROXYPORT, $proxyUrl['port']);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);

        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
}