<?php
namespace Cyphp;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class Seo
{
    protected static $bingBaseUrl = 'https://ssl.bing.com/webmaster/api.svc';

    /**
     *  $batchUrls = [
     *     "siteUrl" => "http://www.qq.com",
     *     "urlList" => ["http://www.qq.com/1.html", "http://www.qq.com/2.html"]
     *  ];
     * @param array $batchUrls
     * @param string $apiKey
     * @return string
     */
    public static function bingUrlSubmit(array $batchUrls, string $apiKey)
    {
        $url = self::$bingBaseUrl . '/json/SubmitUrlbatch?apikey='. $apiKey;
        $client = new Client(['verify' => false]);

        $response = $client->post($url, [
            RequestOptions::JSON => $batchUrls
        ]);

        return $response->getBody()->getContents();
    }

    /**
     * 获取bing提交url剩余额度
     */
    public static function getBingDailyQuota($domain, $apiKey)
    {
        $url = self::$bingBaseUrl . '/json/GetUrlSubmissionQuota?siteUrl=https://'.$domain.'&apikey='.$apiKey;
        $body = Http::curlGet($url);
        $json = json_decode($body, true);

        if (empty($json) || $json['d']['DailyQuota'] < 1){
            return 0;
        }
        return $json['d']['DailyQuota'];
    }
}