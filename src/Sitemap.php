<?php
namespace Cyphp;

class Sitemap
{
    public static function notifyGoogleAndBing($newSitemapUrl)
    {
        $html = file_get_contents('http://www.google.com/ping?sitemap='.urlencode($newSitemapUrl));
        echo "google notify\n";
        echo "{$html}\n";
        $html = file_get_contents('http://www.bing.com/ping?sitemap='.urlencode($newSitemapUrl));
        echo "bing notify\n";
        echo "{$html}\n";
    }
}