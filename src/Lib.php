<?php
namespace Cyphp;

class Lib
{
    public static function date($time = 0)
    {
        return date('Y-m-d H:i:s', ($time > 0 ? $time : time()));
    }

    public static function shortDate($time = 0)
    {
        return date('Y-m-d', ($time > 0 ? $time : time()));
    }
}