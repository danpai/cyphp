<?php
namespace Cyphp;

class Translate
{
    /**
     * 翻译html内容
     * @param string $content
     * @param callable $transFunc
     * @return string
     */
    public static function transHtml(string $content, callable $transFunc)
    {
        $content = trim(str_replace(["\n", "\r", "<p> </p>"], "", $content));
        $content = str_replace('  ', '', $content);
        $content = static::beforeTrans($content);

        preg_match_all('/<.+?\/?>/', $content, $matches);
        $htmlTags = [];
        if (!empty($matches[0])){
            $htmlTags = array_values(array_unique($matches[0]));
        }

        foreach ($htmlTags as $idx => $tag) {
            $content = str_replace($tag, "html{$idx}", $content);
        }

        $waitTransHtmlArr = self::explode1($content);
        $toTransArray = [];
        foreach ($waitTransHtmlArr as $item) {
            //try to exp by ,
            if (strlen($item) > 1800){
                $waitTransHtmlArr1 = self::explode1($content, '，');
                foreach ($waitTransHtmlArr1 as $sitem) {
                    $toTransArray[] = $sitem;
                }
            }else{
                $toTransArray[] = $item;
            }
        }

        //to translate
        $translateHtml = "";
        foreach ($toTransArray as $item) {
            $translateHtml .= $transFunc($item);
        }

        preg_match_all("/[H|h]\s*[T|t]\s*[M|m]\s*[L|l]\s*\d+/is", $translateHtml, $matches);
        if(!empty($matches[0])){
            $matches[0] = array_unique($matches[0]);
            foreach ($matches[0] as $idx => $orgihtMatch) {
                $match = str_replace(' ', '', $orgihtMatch);
                $match = strtolower($match);
                $translateHtml = str_replace($orgihtMatch, $match, $translateHtml);
            }
        }

        //还原html标签占位符
        $len = count($htmlTags);
        for ($idx = $len-1; $idx >= 0; $idx--){
            $translateHtml = str_replace("html{$idx}", $htmlTags[$idx], $translateHtml);
        }
        return $translateHtml;
    }

    protected static function explode1($content, $exp = '。')
    {
        $array = explode($exp, $content);
        $array = array_map('trim', $array);

        $tmpHtml = "";
        $waitTransHtmlArr = [];
        foreach ($array as $item) {
            if (strlen($tmpHtml) + strlen($item) >= 1800){
                $waitTransHtmlArr[] = $tmpHtml;
                $tmpHtml = $item.$exp;
            }else{
                $tmpHtml .= $item.$exp;
            }
        }

        if ($tmpHtml){

            $waitTransHtmlArr[] = rtrim($tmpHtml, $exp);
        }
        return $waitTransHtmlArr;
    }

    /**
     * 翻译前处理
     * @param $html
     * @return array|string|string[]|null
     */
    public static function beforeTrans($html)
    {
        $html = str_replace(['<!-->'], '', $html);
        $pattern = '/<[a-z|A-Z]*><\/[a-z|A-Z]*>/is';
        //删除内容为空的标签
        for ($i = 0; $i < 10; $i++){
            $html = preg_replace($pattern, '', $html);
            preg_match_all($pattern, $html, $matches);
        }
        return $html;
    }
}